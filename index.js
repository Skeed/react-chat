const instanceLocator = "v1:us1:03f52600-d8a1-456f-8a49-34d3d18258a5";
const secretKey = "266baebc-3d70-4fc5-a892-f1d237bc79a2:z8oAwG06P8ZdiNfcwT5Pob4nIz34Jb4qbJ/haM1lqIc=";
const testToken = "https://us1.pusherplatform.io/services/chatkit_token_provider/v1/03f52600-d8a1-456f-8a49-34d3d18258a5/token";
const username = "skeed";
const roomId = "95346d96-5c35-40d3-bda8-877b182028ee";

const chatManager = new Chatkit.ChatManager({
    instanceLocator: instanceLocator,
    userId: username,
    tokenProvider: new Chatkit.TokenProvider({
        url: testToken
    })
});

class Message {
    constructor(name, message, time) {
        this.name = name;
        this.message = message;
        this.time = time;
    }
}

class User {
    constructor(name, presence) {
        this.name = name;
        this.presence = presence;
    }
}

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            messages: [],
            title: "React chat title",
            users: undefined,
        };

        this.sendMessage = this.sendMessage.bind(this)
    }

    sendMessage(message) {
        this.currentUser.sendSimpleMessage({
            text: message,
            roomId: roomId,
        })
    }

    componentDidMount() {
        chatManager.connect().then(currentUser => {
            this.currentUser = currentUser;
            let roomName = this.currentUser.roomStore.rooms[roomId].name;
            this.setState({
                title: roomName,
            });

            currentUser.subscribeToRoomMultipart({
                roomId: roomId,
                hooks: {
                    onPresenceChanged: (action, state) => {
                            this.setState({
                                users: state.presenceStore,
                            })
                        },
                    onMessage: message => {
                        let date = new Date(message.createdAt);
                        let time = ("0" + date.getHours()).slice(-2) + ":" + ("0" + date.getMinutes()).slice(-2);
                        let nickName = message.userStore.users[message.senderId].name;
                        let text = message.parts[0].payload.content;
                        this.setState({
                            messages: [...this.state.messages, new Message(nickName, text, time)]
                        })
                    }
                }
            })
        });
    }

    componentDidUpdate() {
        let chat = document.getElementById("chat");
        chat.scrollTop = chat.scrollHeight;
    }

    render() {
        return (
            <div className="App">
                <Title name={this.state.title} />
                <AppWindows
                    users={this.state.users}
                    messages={this.state.messages}
                    sendMessage={this.sendMessage}
                />
            </div>
        );
    }
}

class Title extends React.Component {
    render() {
        return (
            <div className="title">{this.props.name}</div>
        )
    }
}

class AppWindows extends React.Component {
    render() {
        return (
            <div className="app-windows">
                <span>
                    <UsersWindow users={this.props.users}/>
                </span>
                <span>
                    <ChatWindow messages={this.props.messages}/>
                    <SendForm sendMessage={this.props.sendMessage}/>
                </span>
            </div>
        )
    }
}

class UsersWindow extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidCatch(error, errorInfo) {
        // You can also log the error to an error reporting service
        console.log(error);
        console.log(errorInfo);
    }

    getUsers() {
        let userList = [];
        let users = this.props.users;
        if (users !== undefined) {
            for (let key in users){
                userList = [...userList, new User(key, users[key] === "online")]
            }
            return userList;
        }
        return userList;
    }

    render() {
        let users = this.getUsers();
        console.log(users);
        return (
            <div className="users-panel" >
                <div className="users-blocks">
                    {users.map((user, index) =>
                        <span
                            key={user.name + user.presence + index}
                            className="user-block">
                            <span className={"status " + (user.presence ? "green" : "red")} />
                            {user.name}
                        </span>
                    )}
                </div>
            </div>
        )
    }
}

class ChatWindow extends React.Component {
    render() {
        return (
            <div id="chat" className="chat">
                {this.props.messages.map((message) =>
                    <MessageBlock
                        key={message.name + message.message}
                        name={message.name}
                        message={message.message}
                        time={message.time}
                    />
                )}
            </div>
        )
    }
}

class MessageBlock extends React.Component {
    render() {
        return (
            <div className="message-box">
                <span className="name">{this.props.name}</span>
                <span className="message"><span className="time">{this.props.time}</span>{this.props.message}</span>
            </div>
        )
    }
}

class SendForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            message : "",
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.handleKeyDown = this.handleKeyDown.bind(this);

    }

    handleChange(e) {
        this.setState({
            message: e.target.value,
        })
    }

    handleClick() {
        if (this.state.message !== "") {
            this.props.sendMessage(this.state.message);
            this.setState({
                message: "",
            })
        }
    }

    handleKeyDown(e) {
        if (this.state.message !== "" && e.keyCode === 13) {
            e.preventDefault();
            this.props.sendMessage(this.state.message);
            this.setState({
                message: "",
            });
        }
    }

    render() {
        return (
            <div className="send-form">
                <textarea placeholder="Enter your message..."
                          value={this.state.message}
                          onChange={this.handleChange}
                          onKeyDown={this.handleKeyDown}
                />
                <svg
                    onClick={this.handleClick}
                    className="button-send" version="1.1" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32"
                >
                    <title>Send</title>
                    <path d="M12 20l4-2 14-14-2-2-14 14-2 4zM9.041 27.097c-0.989-2.085-2.052-3.149-4.137-4.137l3.097-8.525 4-2.435 12-12h-6l-12 12-6 20 20-6 12-12v-6l-12 12-2.435 4z" />
                </svg>

            </div>
        )
    }
}

ReactDOM.render(<App />, document.getElementById('root'));
